import { get, post } from '@/api/request'

// 返回三级城市树
export const getCityTree = (data) => get(`/base_api/city/tree`, data)

// 获取编码和子编码
export const getCodeTree = (data) => get(`/base_api/code/tree?appId=2&code=name`)

// 上传图片
export const fileUpload = (data) => post('/up_load_api/attach/upload', data, {
  headers: {
    'content-type': 'multipart/form-data'
  }
})


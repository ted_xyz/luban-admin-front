import { get, post } from '@/api/request'

// 获取师傅列表
export const getWorkerList = (data) => post('/api/admin/worker/search', data)

// 师傅禁用
export const workerDisable = (data) => post('/api/admin/worker/disable', data, {
  headers: {
    'content-type': 'application/x-www-form-urlencoded'
  }
})

// 师傅启用
export const workerEnable = (data) => post('/api/admin/worker/enable', data, {
  headers: {
    'content-type': 'application/x-www-form-urlencoded'
  }
})

// 获取审核列表
export const getWorkerAduit = (data) => post('/api/admin/worker/aduit', data)

// 获取审核详情
export const getWorkerDetail = (data) => post('/api/admin/worker/audit/detail', data, {
  headers: {
    'content-type': 'application/x-www-form-urlencoded'
  }
})

// 获取审核列表
export const auditSave = (data) => post('/api/admin/worker/audit/save', data)


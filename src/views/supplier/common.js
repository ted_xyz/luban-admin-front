// 审核状态
export const auditStatusData = [
  {
    value: null,
    name: '全部'
  },
  {
    value: 0,
    name: '待审核'
  },
  {
    value: 1,
    name: '已审核'
  }
]
// 审批
export const statusData = [
  {
    value: null,
    name: '全部'
  },
  {
    value: 0,
    name: '禁用'
  },
  {
    value: 1,
    name: '启用'
  }
]

// 驳回原因

export const rejectReasonData = [
  {
    value: 1,
    name: '资质不全'
  },
  {
    value: 2,
    name: '营业执照照片不清晰'
  },
  {
    value: 3,
    name: '其他'
  }
]

// 审核
export const auditStatusMap = {
  0: '未审核',
  1: '审核通过',
  2: '驳回'
}

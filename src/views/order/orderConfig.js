// 订单状态
export const orderStatus = {
  '10': '抢单成功',
  '20': '签到成功',
  '30': '服务完成待结算',
  '40': '完成'
}
export const statusData = [
  {
    value: null,
    name: '全部'
  },
  {
    value: 0,
    name: '待抢'
  },
  {
    value: 1,
    name: '已抢'
  }
]
